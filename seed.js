// init
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// redis init
var redis = require('redis');
var client = redis.createClient();
// error handling
client.on("error", function(err) {
	console.log("Err: " + err);
});

// server listening
var port = 55555;
http.listen(port, function() {
	console.log("listening at *:" + port);
});

// first time generate and store in Redis
var seed = Math.random() * 99;
client.set('seed', seed);
console.log("<seed>:" + seed);

// while connected to client,
io.on('connection', function(socket) {

	// initiate first seed
	socket.on('req-seed', function(data) {
		socket.emit('res-seed', seed);
	});

	// matching guess with seed
	socket.on('req-verify', function(data) {

		client.get('seed', function(err, reply) {
			if(parseFloat(reply).toFixed(0) == parseFloat(reply).toFixed(0)) { // if match,
				// announce isCracked true
				io.emit('isCracked', true);
				// generate new seed
				seed = Math.random() * 99;
				client.set('seed', seed);
				console.log("<seed*>:" + seed);
				// announce the winner
				socket.emit('res-verify', "success");
			}
		});
	});
});

// exit signal call
process.on('SIGINT', function() {
	console.log("seed program exits!");
	process.exit();
});