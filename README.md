Guess Game Server 
=================

A simple random numbers matching implementation on server. The program works as follow: 

1. The server generates a random number called 'seed', and store it inside Redis. 

2. If there is a request of seed ('req-seed'), Server will sent it with ('res-seed'). 

3. If a Client claims he/she cracked the seed ('req-verify'), match the seed with theirs. If it match, give 1 point to that Client. 

# Howto Setup

* Download, setup and run Redis. 

* Resolve dependencies.

```console
$ npm install --save
```

* Run the server. 

```console
$ node seed.js
```

# High-level architecture

![ScreenShot](img/Capture.PNG)

# Author

hadrihl // hadrihilmi@gmail.com // mdnorhadrih@wou.edu.my

Part of assignment of TSNP303/03 Distributed Systems. Semester 2019/May. Wawasan Open University. 